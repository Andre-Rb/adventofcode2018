use std::fs;

struct FrequecyChange {
	is_positive: bool,
	amount: u32,
}

const PLUS_CHAR: char = '+';
const MINUS_CHAR: char = '-';
const FILENAME: &str = "res/input";

fn main() {
	println!("In file {}", FILENAME);

	let contents = fs::read_to_string(FILENAME).expect("Something went wrong reading the file");
	let chars_vect: Vec<&str> = contents.split_whitespace().collect();

	let is_valid = check_conformity(&chars_vect);
	assert!(is_valid, "Input are not conform !");

	let mut list_of_frequencychanges: Vec<FrequecyChange> = Vec::new();
	populate_list(&mut list_of_frequencychanges, chars_vect);
	
	let final_frequency = compute(&list_of_frequencychanges);
	println!("Final frequency is {}", final_frequency);
}

fn populate_list(list: &mut Vec<FrequecyChange>, chars_vect: Vec<&str>) {
	for a_line in chars_vect {
		let mut iterator = a_line.chars();
		let first_char = iterator.next().unwrap();
		let remaining: String = iterator.collect();

		let new_freq_change = frequency_change_constructor(first_char, &remaining);

		list.push(new_freq_change);
	}
}

fn compute(input: &Vec<FrequecyChange>) -> i32 {
	let starting_frequency = 0;
	let mut latest_frequency = starting_frequency;

	for a_change in input {
		latest_frequency = update_frequency(&a_change, latest_frequency);
	}
	latest_frequency
}

fn frequency_change_constructor(sign_char: char, amount: &str) -> FrequecyChange {
	let positive = if sign_char == PLUS_CHAR {
		true
	} else if sign_char == MINUS_CHAR {
		false
	} else {
		panic!("Unexpected sign !")
	};

	let parsed_amount = amount.parse().unwrap();

	FrequecyChange {
		is_positive: positive,
		amount: parsed_amount,
	}
}

fn check_conformity(input: &[&str]) -> bool {
	let mut number_of_okay_lines = 0;
	let mut number_of_bad_lines = 0;
	let mut number_lines = 0;

	for c in input {
		number_lines += 1;

		let first = c.chars().next().unwrap();
		if first == PLUS_CHAR || first == MINUS_CHAR {
			number_of_okay_lines += 1;
		} else {
			number_of_bad_lines += 1;
		}
	}
	println!(
		"There is {} lines, with {} valid ones and {} invalid ones",
		number_lines, number_of_okay_lines, number_of_bad_lines
	);
	number_lines == number_of_okay_lines
}
fn update_frequency(input: &FrequecyChange, mut frequency_to_update: i32) -> i32 {
	if input.is_positive {
		frequency_to_update += input.amount as i32;
	} else {
		frequency_to_update -= input.amount as i32;
	}

	frequency_to_update
}
