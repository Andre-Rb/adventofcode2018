use std::collections::HashMap;

#[derive(Eq, PartialEq, Hash)]
pub enum IdType {
    OnlyThreeIdentical,
    OnlyTwoIdentical,
    Irrelevant,
    Both,
}

pub fn compute_mini_hash(stats: &HashMap<IdType, i32>) -> i32 {
    let twos_ref = stats
        .get(&IdType::OnlyTwoIdentical)
        .expect("Getting twos failed");
    let both_ref = stats.get(&IdType::Both).expect("Getting Both failed");
    let threes_ref = stats
        .get(&IdType::OnlyThreeIdentical)
        .expect("Getting Both failed");

    let total_id_with_twos = *twos_ref + *both_ref;
    let total_id_with_threes = *threes_ref + *both_ref;

    total_id_with_threes * total_id_with_twos
}

pub fn compile_stats(stats: &mut HashMap<IdType, i32>, inputs: &Vec<&str>) {
    for a_box_id in inputs {
        let letter_stats = count_letter(a_box_id);
        let id_type = read_id_type(&letter_stats);
        let count = stats.entry(id_type).or_insert(0);
        *count += 1;
    }
}

pub fn count_letter(word: &str) -> HashMap<&str, i32> {
    let mut hashmap = HashMap::new();

    for char in word.split_terminator("") {
        if char != "" {
            let count = hashmap.entry(char).or_insert(0);
            *count += 1;
        }
    }

    hashmap
}

fn _print_hashmap(input: &HashMap<&str, i32>) {
    println!("Here goes the print :");

    for a_key in input.keys() {
        println!("\t Letter '{}' -> {}", a_key, input.get(a_key).unwrap())
    }
}

pub fn read_id_type(input: &HashMap<&str, i32>) -> IdType {
    let mut found_two = false;
    let mut found_three = false;

    for a_value in input.values() {
        match a_value {
            2 => found_two = true,
            3 => found_three = true,
            _ => (),
        }
    }

    if found_two && found_three {
        IdType::Both
    } else if found_two {
        IdType::OnlyTwoIdentical
    } else if found_three {
        IdType::OnlyThreeIdentical
    } else {
        IdType::Irrelevant
    }
}
