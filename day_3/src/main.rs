extern crate utilities_lib;

fn main() {
    let input = load_input();
    println!("{}", input);
    let splitted = utilities_lib::split_on_CR(&input);

    for elem in splitted {
        println!("{}",elem );
    }
}

struct Claim {
    id: i32,
    left_margin: i32,
    top_margin: i32,
    width: i32,
    lenght: i32,
}

impl Claim {
    fn area(&self) -> (i32) {
        self.width * self.lenght
    }
    fn load_from_string(input: &str) {}

    fn print(&self) {
        println!("#{}{}{}", self.id, self.left_margin, self.top_margin);
    }
}

fn load_input() -> String {
    utilities_lib::load_file("res/input")
}

#[test]
fn loaded_properly() {
    assert!(!load_input().is_empty());
}

#[test]
fn claim_area() {
    let a_claim = Claim {
        id: 0,
        left_margin: 0,
        top_margin: 0,
        width: 10,
        lenght: 10,
    };

    assert!(a_claim.area() == 100);
}

#[test]
fn count_line_splitted() {
    let input = load_input();
    let splitted = utilities_lib::split_on_CR(&input);
    let len = splitted.len();
    assert!(len == 1230);
}
